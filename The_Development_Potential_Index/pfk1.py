#评分卡


# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import datetime
import time


# ## part1 每个二级指标的得分

# In[2]:


#评分模型-单个区县-
## part1 每个二级指标的得分
#一产占比
def pfk_yczb(gdp1):
    df = 5.20             
    if 0 < gdp1 < 0.02 :
        gdp1_df = 0.2*df
    elif 0.02 <= gdp1 < 0.06 :
        gdp1_df = 0.4*df
    elif 0.06 <= gdp1 < 0.18 :
        gdp1_df = 0.6*df
    elif 0.18 <= gdp1 < 0.24 :
        gdp1_df = 0.8*df
    elif 0.24 <= gdp1 < 1 :
        gdp1_df = df
    else:
        gdp1_df = 0.1*df
    return gdp1_df


# In[3]:


#农作物播种面积
def pfk_nzw(nzw):
    df = 6.50              
    if 0 < nzw <= 100:
        nzw_df = 0.2*df
    elif 100 < nzw <= 250:
        nzw_df = 0.4*df
    elif 250 < nzw <= 650:
        nzw_df = 0.6*df
    elif 650 < nzw <= 1100:
        nzw_df = 0.8*df
    elif nzw > 1100:
        nzw_df = df
    else:
        nzw_df = 0.1*df
    return nzw_df


# In[4]:


#生猪出栏量(农畜)
def pfk_szcl(szcl):
    df = 2.60
    if 0 < szcl <= 10:
        szcl_df = 0.2*df
    elif 10 < szcl <= 50:
        szcl_df = 0.4*df
    elif 50 < szcl <= 250:
        szcl_df = 0.6*df
    elif 250 < szcl <= 500:
        szcl_df = 0.8*df
    elif  szcl > 500:
        szcl_df = df
    else:
        szcl_df = 0.1*df
    return szcl_df


# In[5]:


#人均农业总产值
#人均农业总产值
def pfk_rjgdp(rjgdp):
    df = 6.50
    if 1 < rjgdp <= 2700:
        rjgdp_df = 0.2*df
    elif 2700 < rjgdp <= 4700:
        rjgdp_df = 0.4*df
    elif 4700 < rjgdp <= 8000:
        rjgdp_df = 0.6*df
    elif 8000 < rjgdp <= 13000:
        rjgdp_df = 0.8*df
    elif  rjgdp > 13000:
        rjgdp_df = df
    else:
        rjgdp_df = 0.1*df
    return rjgdp_df


# In[6]:


#亩均农业生产总值
def pfk_mjny(mjny):
    df = 5.20
    if 1 <= mjny <= 1600:
        mjny_df = 0.2*df
    elif 1600 < mjny <= 2500:
        mjny_df = 0.4*df
    elif 2500 < mjny <= 5000:
        mjny_df = 0.6*df
    elif 5000 < mjny <= 8000:
        mjny_df = 0.8*df
    elif  mjny > 8000:
        mjny_df = df
    else:
        mjny_df = 0.1*df
    return mjny_df


# In[7]:


#地标产品
def pfk_db(db):
    df = 9.60
    if 1 <= db <= 2:
        db_df = 0.2*df
    elif 2 < db <= 5:
        db_df = 0.4*df
    elif 5 < db <= 12:
        db_df = 0.6*df
    elif 12 < db <= 21:
        db_df = 0.8*df
    elif 21 < db <= 100:
        db_df = df
    else:
        db_df = 0.1*df
    return db_df


# In[8]:


#国家级龙头企业
def pfk_gjlt(gjlt):
    df = 9.60
    if 0 < gjlt <= 1.5:
        gjlt_df = 0.2*df
    elif 1.5 < gjlt <= 2.3:
        gjlt_df = 0.4*df
    elif 2.3 < gjlt <= 5.1:
        gjlt_df = 0.6*df
    elif 5.1 < gjlt <= 11:
        gjlt_df = 0.8*df
    elif 11 < gjlt <= 100:
        gjlt_df = df
    else:
        gjlt_df = 0.1*df
    return gjlt_df


# In[9]:


#省级龙头企业
def pfk_sjlt(sjlt):
    df = 4.80
    if 1 <= sjlt <= 6:
        sjlt_df = 0.2*df
    elif 6 < sjlt <= 20:
        sjlt_df = 0.4*df
    elif 20 < sjlt <= 60:
        sjlt_df = 0.6*df
    elif 60 < sjlt <= 98:
        sjlt_df = 0.8*df
    elif 98 < sjlt <= 1000:
        sjlt_df = df
    else:
        sjlt_df = 0.1*df
    return sjlt_df


# In[10]:


#财政收入
def pfk_czsr(czsr):
    df = 3.60
    if 1 < czsr <= 30:
        czsr_df = 0.2*df
    elif 30 < czsr <= 70:
        czsr_df = 0.4*df
    elif 70 < czsr <= 220:
        czsr_df = 0.6*df
    elif 220 < czsr <= 700:
        czsr_df = 0.8*df
    elif  czsr > 700:
        czsr_df = df
    else:
        czsr_df = 0.1*df
    return czsr_df


# In[11]:


#财政支出
def pfk_czzc(czzc):
    df = 3.00
    if 1 <= czzc <= 125:
        czzc_df = 0.2*df
    elif 125 < czzc <= 255:
        czzc_df = 0.4*df
    elif 255 < czzc <= 530:
        czzc_df = 0.6*df
    elif 530 < czzc <= 975:
        czzc_df = 0.8*df
    elif  czzc > 975:
        czzc_df = df
    else:
        czzc_df = 0.1*df
    return czzc_df


# In[12]:


#财政盈余
def pfk_czyk(czyk):
    df = 3.40
    if -10000 <= czyk <= -450:
        czyk_df = 0.2*df
    elif -450 < czyk <= -310:
        czyk_df = 0.4*df
    elif -310 < czyk <= -140:
        czyk_df = 0.6*df
    elif -140 < czyk <= -80:
        czyk_df = 0.8*df
    elif  -80 < czyk < 10000:
        czyk_df = df
    else:
        czyk_df = 0.1*df
    return czyk_df


# In[13]:


#中央衔接资金
def pfk_zyxj(zyxj):
    df = 5.20
    if 0 <= zyxj <= 600:
        zyxj_df = 0.2*df
    elif 600 < zyxj <= 3500:
        zyxj_df = 0.4*df
    elif 3500 < zyxj <= 36000:
        zyxj_df = 0.6*df
    elif 36000 < zyxj <= 81000:
        zyxj_df = 0.8*df
    elif  zyxj > 81000:
        zyxj_df = df
    else:
        zyxj_df = 0.1*df
    return zyxj_df


# In[14]:


#省级衔接资金
def pfk_sjxj(sjxj):
    df = 4.80
    if 0 <= sjxj <= 700:
        sjxj_df = 0.2*df
    elif 700 < sjxj <= 4000:
        sjxj_df = 0.4*df
    elif 4000 < sjxj <= 20000:
        sjxj_df = 0.6*df
    elif 20000 < sjxj <= 46000:
        sjxj_df = 0.8*df
    elif  sjxj > 46000:
        sjxj_df = df
    else:
        sjxj_df = 0.1*df    
    return sjxj_df


# In[15]:


#覆盖广度
def pfk_fggd(fggd):
    df = 8.00
    if 1 <= fggd <= 230:
        fggd_df = 0.2*df
    elif 230 < fggd <= 260:
        fggd_df = 0.4*df
    elif 260 < fggd <= 300:
        fggd_df = 0.6*df
    elif 300 < fggd <= 320:
        fggd_df = 0.8*df
    elif  fggd > 320:
        fggd_df = df
    else:
        fggd_df = 0.1*df 
    return fggd_df


# In[16]:


#支付区间
def pfk_zfqj(zfqj):
    df = 2.50
    if 1 < zfqj <= 222:
        zfqj_df = 0.2*df
    elif 222 < zfqj <= 242:
        zfqj_df = 0.4*df
    elif 242 < zfqj <= 287:
        zfqj_df = 0.6*df
    elif 287 < zfqj <= 317:
        zfqj_df = 0.8*df
    elif  zfqj > 317:
        zfqj_df = df
    else:
        zfqj_df = 0.1*df 
    return zfqj_df


# In[17]:


#保险区间
def pfk_bxqj(bxqj):
    df = 2.50
    if 1 <= bxqj <= 320:
        bxqj_df = 0.2*df
    elif 320 < bxqj <= 370:
        bxqj_df = 0.4*df
    elif 370 < bxqj <= 450:
        bxqj_df = 0.6*df
    elif 450 < bxqj <= 500:
        bxqj_df = 0.8*df
    elif  bxqj > 500:
        bxqj_df = df
    else:
        bxqj_df = 0.1*df 
    return bxqj_df


# In[18]:


#投资区间
def pfk_tzqj(tzqj):
    df = 2.50
    if 1 < tzqj <= 182:
        tzqj_df = 0.2*df
    elif 182 < tzqj <= 200:
        tzqj_df = 0.4*df
    elif 200 < tzqj <= 256:
        tzqj_df = 0.6*df
    elif 256 < tzqj <= 300:
        tzqj_df = 0.8*df
    elif  tzqj > 300:
        tzqj_df = df
    else:
        tzqj_df = 0.1*df 
    return tzqj_df


# In[19]:


#信贷区间
def pfk_xdqj(xdqj):
    df = 2.50
    if 1 < xdqj <= 150:
        xdqj_df = 0.2*df
    elif 150 < xdqj <= 170:
        xdqj_df = 0.4*df
    elif 170 < xdqj <= 192:
        xdqj_df = 0.6*df
    elif 177 < xdqj <= 200:
        xdqj_df = 0.8*df
    elif  xdqj > 200:
        xdqj_df = df
    else:
        xdqj_df = 0.1*df 
    return xdqj_df


# In[20]:


#数字化指数
def pfk_szh(szh):
    df = 12.00
    if 1 < szh <= 276:
        szh_df = 0.2*df
    elif 276 < szh <= 288:
        szh_df = 0.4*df
    elif 288 < szh <= 310:
        szh_df = 0.6*df
    elif 310 < szh <= 320:
        szh_df = 0.8*df
    elif  szh > 320:
        szh_df = df
    else:
        szh_df = 0.1*df 
    return szh_df


# ## part2 每个一级指标的得分

# In[21]:


## part2 每个一级指标的得分
## 产业结构得分
def pdf_cyjg(row):
    yczb_df = pfk_yczb(row["一产占比"])
    nzw_df = pfk_nzw(row["农作物播种面积（千公顷）"])
    szcl_df = pfk_szcl(row["肉猪出栏头数（万只）"])
    rjgdp_df = pfk_rjgdp(row["人均农业总产值（元/人）"])
    mjny_df = pfk_mjny(row["亩均农业总产值（元/亩）"])
    
    cyjgdf = yczb_df + nzw_df + szcl_df + rjgdp_df+ mjny_df
    
    return cyjgdf


# In[22]:


## 产业竞争力得分
def pdf_cyjzl(row):
    db_df = pfk_db(row["地理产品"])
    gjlt_df = pfk_gjlt(row["国家级龙头企业"])
    sjlt_df = pfk_sjlt(row["省级龙头企业"])
   
    cyjzldf = db_df + gjlt_df + sjlt_df
    
    return cyjzldf


# In[23]:


## 财政资金得分
def pdf_czzj(row):
    czsr_df = pfk_czsr(row["财政收入（亿元）"])
    czzc_df = pfk_czzc(row["财政支出（亿元）"])
    czyk_df = pfk_czyk(row["财政盈余（亿元）"])
    zyxj_df = pfk_zyxj(row["中央衔接资金（万元）"])
    sjxj_df = pfk_sjxj(row["省级衔接资金（万元）"])
        
    czzjdf = czsr_df + czzc_df + czyk_df + zyxj_df + sjxj_df 
    
    return czzjdf


# In[24]:


## 数字支付覆盖广度得分
def pdf_fggd(row):
    fggd_df = pfk_fggd(row["覆盖广度指数"])
        
    fggddf = fggd_df
    
    return fggddf


# In[25]:


## 金融使用深度得分   
def pdf_sysd(row):
    zfqj_df = pfk_zfqj(row["支付使用指数"])
    bxqj_df = pfk_bxqj(row["保险使用指数"])
    tzqj_df = pfk_tzqj(row["投资使用指数"])
    xdqj_df = pfk_xdqj(row["信贷使用指数"])
  
    sysddf = zfqj_df + bxqj_df + tzqj_df + xdqj_df
    
    return sysddf


# In[26]:


## 数字化得分
def pdf_szh(row):
    szh_df = pfk_szh(row["数字化指数"])
    
    szhdf = szh_df
    
    return szhdf


# In[27]:


## 总分
def pdf_ZF(row):
    yczb_df = pfk_yczb(row["一产占比"])
    nzw_df = pfk_nzw(row["农作物播种面积（千公顷）"])
    szcl_df = pfk_szcl(row["肉猪出栏头数（万只）"])
    rjgdp_df = pfk_rjgdp(row["人均农业总产值（元/人）"])
    mjny_df = pfk_mjny(row["亩均农业总产值（元/亩）"])
    
    db_df = pfk_db(row["地理产品"])
    gjlt_df = pfk_gjlt(row["国家级龙头企业"])
    sjlt_df = pfk_sjlt(row["省级龙头企业"])
    
    czsr_df = pfk_czsr(row["财政收入（亿元）"])
    czzc_df = pfk_czzc(row["财政支出（亿元）"])
    czyk_df = pfk_czyk(row["财政盈余（亿元）"])
    zyxj_df = pfk_zyxj(row["中央衔接资金（万元）"])
    sjxj_df = pfk_sjxj(row["省级衔接资金（万元）"])
    
    fggd_df = pfk_fggd(row["覆盖广度指数"])
    
    zfqj_df = pfk_zfqj(row["支付使用指数"])
    bxqj_df = pfk_bxqj(row["保险使用指数"])
    tzqj_df = pfk_tzqj(row["投资使用指数"])
    xdqj_df = pfk_xdqj(row["信贷使用指数"])
    
    szh_df = pfk_szh(row["数字化指数"])
    
    ZF = yczb_df + nzw_df + szcl_df + rjgdp_df + mjny_df + db_df + gjlt_df + sjlt_df + czsr_df + czzc_df + czyk_df + zyxj_df + sjxj_df + fggd_df + zfqj_df + bxqj_df + tzqj_df + xdqj_df + szh_df
    
    return ZF







